import Phaser from "phaser";
import SceneLogoLoad from "./Scenes/sceneLogoLoad.js";
import CampusGrid from "./Scenes/CampusGrid.js";

const rest = require('rest');

//const scaleMode = 'FIT' | 'SMOOTH'

const DEFAULT_WIDTH = 1024
const DEFAULT_HEIGHT = 576
const MAX_WIDTH = 1536
const MAX_HEIGHT = 864
let SCALE_MODE = 'SMOOTH' // FIT OR SMOOTH

var game;
var isMobile = navigator.userAgent.indexOf("Mobile");
if (isMobile == -1) {
    isMobile = navigator.userAgent.indexOf("Tablet");
}

if (isMobile == -1) {
  var config = {
      type: Phaser.AUTO,
      parent: 'phaser-game',
      width: window.innerWidth,
      height: window.innerHeight,
      scale: {
          autoCenter: Phaser.Scale.CENTER_HORIZONTALLY
      },
      input: {
          queue: true
      },
      dom: {
          createContainer: true
      },
      scene: [
        //PixpikLoad,
        SceneLogoLoad,
        CampusGrid
        ]
  };
} else {
  var config = {
  type: Phaser.AUTO,
  parent: 'phaser-game',
  scale:{
      mode: Phaser.Scale.NONE,
      width: window.innerWidth * window.devicePixelRatio,
      height: window.innerHeight * window.devicePixelRatio,
      zoom: 1 / window.devicePixelRatio
  },
  dom: {
      createContainer: true
  },
  scene: [
      //PixpikLoad,
      SceneLogoLoad, 
      CampusGrid
    ]
};
}

window.addEventListener('load', () => {
    game = new Phaser.Game(config)
  
    const resize = () => {
      const w = window.innerWidth
      const h = window.innerHeight
  
      let width = DEFAULT_WIDTH
      let height = DEFAULT_HEIGHT
      let maxWidth = MAX_WIDTH
      let maxHeight = MAX_HEIGHT
      let scaleMode = SCALE_MODE
  
      let scale = Math.min(w / width, h / height)
      let newWidth = Math.min(w / scale, maxWidth)
      let newHeight = Math.min(h / scale, maxHeight)
  
      let defaultRatio = DEFAULT_WIDTH / DEFAULT_HEIGHT
      let maxRatioWidth = MAX_WIDTH / DEFAULT_HEIGHT
      let maxRatioHeight = DEFAULT_WIDTH / MAX_HEIGHT
  
      // smooth scaling
      let smooth = 1
      if (scaleMode === 'SMOOTH') {
        const maxSmoothScale = 1.15
        const normalize = (value, min, max) => {
          return (value - min) / (max - min)
        }
        if (width / height < w / h) {
          smooth =
            -normalize(newWidth / newHeight, defaultRatio, maxRatioWidth) / (1 / (maxSmoothScale - 1)) + maxSmoothScale
        } else {
          smooth =
            -normalize(newWidth / newHeight, defaultRatio, maxRatioHeight) / (1 / (maxSmoothScale - 1)) + maxSmoothScale
        }
      }
  
      // resize the game
      game.scale.resize(newWidth * smooth, newHeight * smooth)
  
      // scale the width and height of the css
      game.canvas.style.width = newWidth * scale + 'px'
      game.canvas.style.height = newHeight * scale + 'px'
  
      // center the game with css margin
      game.canvas.style.marginTop = `${(h - newHeight * scale) / 2}px`
      game.canvas.style.marginLeft = `${(w - newWidth * scale) / 2}px`
    }
    window.addEventListener('resize', event => {
      resize()
    })
    resize()
  })
//game = new Phaser.Game(config);


