import Bar from "./../Classes/bar.js";
import Building from "./../Classes/Building.js";
import AlignGrid from "./../util/alignGrid.js";
import FormUtil from "./../util/FormUtil.js";
import grid from "./../assets/Resources/grid.json";
import textForm from "./../HTML/text.html";

export default class CampusGrid extends Phaser.Scene {
    constructor(test) {
        super({
            key: 'CampusGrid'
        });
    }
    preload() {
        this.cameras.main.setBackgroundColor('0xff00ff');
    }

    create(){

        this.alignGrid = new AlignGrid({
            scene: this,
            rows: 11,
            cols: 11
        });
        this.alignGrid.showNumbers();
       //

        this.form = this.add.dom(0, 0).createFromHTML(textForm);
       // this.formUtil.scaleToGameW("myText", .3);
        //this.formUtil.placeElementAt(16, 'myText', true);

        var keyObj = this.input.keyboard.addKey('W');  // Get key object
        keyObj.on('down', (event)=>{
            console.log('pressing w');
            //console.log(this.alignGrid.getTextValue('myText'));
            console.log(this.alignGrid.getTextAreaValue('myText'));
        });

        console.log('STARTING');
        console.log(grid);
        grid.forEach((building)=>{
            const build = new Building({scene:this, x:building.posX, y:building.posY, title:building.title, admin: true});
        })
        
    }

    update(){
        this.alignGrid.updateGrid();
        this.alignGrid.placeAtIndex(14, this.form);
        this.alignGrid.scaleFormToGameW('myText', 0.5);
    }
}