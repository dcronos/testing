import logoImg from "./../assets/pixpik_logo.png";
import default_building from "./../assets/Buildings/default_building.png";
import Bar from "./../Classes/bar";

class SceneLogoLoad extends Phaser.Scene {
    constructor(scene) {
        super({
            key: 'SceneLogoLoad'
        });
    }
    
    preload() {
        this.sceneProp = this.scene;
        console.log(this.scene);
        console.log(this.sceneProp);
        this.load.image("logo", logoImg);
        this.load.image("default_building", default_building);
        var config = this.sys.game.config;
        this.cameras.main.setBackgroundColor('0xff00ff');
        this.bar=new Bar({scene:this,x:config.width/2,y:config.height/2});
    	this.progText=this.add.text(config.width/2, config.height/2,"0%",{color:'#000000',fontSize:config.width/20});
        //this.progText=this.add.text(100, 100,"0%",{color:'#000000',fontSize:30});
        this.progText.setOrigin(0.5,0.5);
        //this.load.on('progress', this.onProgress, this);
        //this.load.image("logo", 'images/pixpik_logo.png');
        this.load.on('start', () => {
            
            console.log('STARTING');
         });

         this.load.on('progress', (value) => {
            var per=Math.floor(value*100);
            console.log('LOAD: ' + per);
            this.progText.setText(per+"%");
        }, this);

        this.load.on('complete', this.loadComplete, this);
    }
    
    create() {
        var logo = this.add.image(400, 150, 'logo');
    }

    loadComplete(){
        console.log('timer start');
        this.time.delayedCall(1000, this.timeout, [], this);
    }

    timeout(){
        console.log('timer end');
        this.scene.start("CampusGrid");
    }
}

export default SceneLogoLoad;