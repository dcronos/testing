export default class Building extends Phaser.GameObjects.Container {
    constructor(config) {
        super(config.scene);
        this.scene = config.scene;

        if (config.x) {
            this.x = config.x;
        }
        if (config.y) {
            this.y = config.y;
        }

        if(config.image){
            const build = this.scene.add.image(0, 0, config.image);
            ///logo.setPosition(0, -logo.height);
            this.add(build);
        } else {
            const build = this.scene.add.image(0, 0, 'default_building');
            ///logo.setPosition(0, -logo.height);
            this.add(build);
        }

        if(config.title){
            this.title = this.scene.add.text(this.x/2, this.y/2, config.title, {color:'#000000',fontSize:30});
        }else {
            this.title = this.scene.add.text(this.x/2, this.y/2, "Classroom", {color:'#000000',fontSize:30});
        }

        if(config.admin){
            
        }

        this.scene.add.existing(this);
    }
}